variable "subnet" {
  description = "Подсеть, в которой нужно расположить ресурсы"
  type        = any
}

variable "img_id" {
  description = "Используемый образ"
  type        = string
}
