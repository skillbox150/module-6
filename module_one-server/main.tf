# Внешний адрес для ВМ
resource "yandex_vpc_address" "addr" {
  name = "OneServer external addr"
  external_ipv4_address {
    zone_id = var.subnet.zone
  }
}

# читаем содержимое файла user.data
data "local_file" "user_data" {
    filename = "${path.module}/user.data"
}

# Запускаем инстанс
resource "yandex_compute_instance" "os-wb-01" {
  name = "os-wb-01"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 1
  }

  network_interface {
    subnet_id       = var.subnet.id
    nat             = true
    nat_ip_address  = yandex_vpc_address.addr.external_ipv4_address[0].address
  }

  # прерываемая
  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id = var.img_id
      size = 15
    }
  }

  metadata = {
    user-data = "${data.local_file.user_data.content}"
    serial-port-enable = "1"
  }
}

# Выведем IP адрес сервера
output "zone-b" {
  description = "Elatic IP address assigned to our WebSite"
  value       = var.subnet
}

output "my_web_site_ip" {
  description = "Внешний адрес сервера"
  value       = yandex_vpc_address.addr.external_ipv4_address[0].address
}
