terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.74"
    }
  }
}

variable "folder_id" {
  description = "id currnet folder"
  default     = "b1glidjuecmg0s63lpnn" 
}

provider "yandex" {
  cloud_id  = "b1gn2hda8rikkkp7uh0l"
  folder_id = var.folder_id
  zone      = "ru-central1-b"
}

