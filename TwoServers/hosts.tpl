[backend_servers]
  %{ for instance in instances ~}
  ${instance.name} ansible_host=${instance.network_interface[0].nat_ip_address} ansible_user=eshlygin ansible_ssh_private_key=~/.ssh/id_ed25519
  %{ endfor ~}

