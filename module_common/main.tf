resource "yandex_vpc_network" "skillbox" {
  name = "skillbox"
}

# Создаем подсети
resource "yandex_vpc_subnet" "skillbox" {
  for_each = var.zones

  name           = each.value["name"]
  zone           = each.value["zone"]
  network_id     = yandex_vpc_network.skillbox.id
  v4_cidr_blocks = [each.value["cidr"]]
}

output "subnets" {
  description = "Созданные подсети"
  value       = yandex_vpc_subnet.skillbox
}

# Ищем образ с последней версией Ubuntu
data "yandex_compute_image" "ubuntu-2004-lts" {
  family = "ubuntu-2004-lts"
}

output "default_img" {
  description = "Образ по умолчанию"
  value       = data.yandex_compute_image.ubuntu-2004-lts
}
