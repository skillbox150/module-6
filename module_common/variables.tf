variable "zones" {
  description = "Зоны, в которых будут расположены ресурсы"
  type        = map
  default = {
    zone_a = {
      name = "skillbox-a"
      zone = "ru-central1-a"
      cidr = "192.168.20.0/24"
    }
    zone_b = {
      name = "skillbox-b"
      zone = "ru-central1-b"
      cidr = "192.168.21.0/24"
    }
    zone_c = {
      name = "skillbox-c"
      zone = "ru-central1-c"
      cidr = "192.168.22.0/24"
    }
  }
}
