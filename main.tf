module "common" {  
  source  = "./module_common"
}

module "one-server" {  
  source  = "./module_one-server"
  subnet  = module.common.subnets.zone_b
  img_id  = module.common.default_img.id
}

resource "local_file" "template_inventory" {
  content     = templatefile("./OneServer/hosts.tpl", {
      server_ip = module.one-server.my_web_site_ip
  })
  filename = "./OneServer/hosts"
}

# Выведем IP адрес сервера
output "my_web_site_ip" {
  description = "Elatic IP address assigned to our WebSite"
  value       = module.one-server.my_web_site_ip
}

# два бэкэнда и балансировщик нагрузки
module "two-servers" {
  source  = "./module_two-servers"
  subnets  = module.common.subnets
  img_id  = module.common.default_img.id
}

# IP адрес балансировщика
output "balancer_ip" {
  description = "Адрес балансировщика"
  value       = module.two-servers.balancer_ip
}

# Заполним инвентори файл
resource "local_file" "template_inventory_two-servers" {
  content     = templatefile("./TwoServers/hosts.tpl", {
    instances = module.two-servers.vm_group.instances
  })
  filename = "./TwoServers/hosts"
}
